require("dotenv").config();
const express = require("express");
const app = express();
const port = 3000;
const router = require("./routes/index");

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(router);

// console.log(process.env.NODE_ENV);

if (process.env.NODE_ENV == "development") {
    app.listen(port, () => {
        console.log(`Server berjalan di Port ${port}`);
    });
}

module.exports = app;
