const {Todo, sequelize} = require("../models");

class TodolistController {
    static findAll = async (req, res) => {
        try {
            const data = await Todo.findAll({
                where: {
                    status: "active",
                },
                order: sequelize.literal("(id)"),
            });

            res.json(data);
        } catch (err) {
            res.status(500).json({
                message: "Internal Server Error",
            });
            throw err;
        }
    };

    static findOne = async (req, res) => {
        const {id} = req.params;

        try {
            const data = await Todo.findOne({
                where: {
                    id,
                },
            });
            // console.log(data);

            if (data) {
                res.status(200).json(data);
            } else {
                res.status(404).json({
                    message: "To Do not found.",
                });
            }
        } catch (err) {
            res.status(500).json({
                message: "Internal Server Error",
            });
            throw err;
        }
    };

    static createTodo = async (req, res) => {
        try {
            const data = await Todo.create(req.body);
            // console.log(req.body);

            res.status(201).json({
                message: "Create To Do Successfully",
                data: data,
            });
        } catch (err) {
            if ("SequelizeValidationError") {
                res.status(400).json({
                    message: "Validation Error, Failed Create To Do",
                });
            } else {
                res.status(500).json({
                    message: "Internal Server Error",
                });
            }
            throw err;
        }
    };

    static softDelete = async (req, res) => {
        try {
            const {id} = req.params;

            const data = await Todo.update(
                {
                    status: "inactive",
                },
                {
                    where: {
                        id,
                    },
                }
            );

            const findData = await Todo.findOne({
                where: {
                    id,
                },
            });

            if (data[0] === 1) {
                res.status(200).json({
                    message: "Deleted To Do Successfully",
                    dataDelete: findData,
                });
            } else {
                res.status(404).json({
                    message: "To Do Not Found",
                });
            }
        } catch (err) {
            res.status(500).json({
                message: "Internal Server Error",
            });
            throw err;
        }
    };
}

module.exports = TodolistController;
