const express = require("express");
const router = express.Router();
const TodolistController = require("../controllers/todolistController");

router.get("/todo", TodolistController.findAll);
router.get("/todo/:id", TodolistController.findOne);
router.post("/todo", TodolistController.createTodo);
router.delete("/todo/:id", TodolistController.softDelete);

module.exports = router;
