const app = require("../app");
const request = require("supertest");

describe("API /todo", () => {
    it("List All Todo GET /todo", (done) => {
        request(app)
            .get("/todo")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                const firstResult = response.body[0];
                const secondResult = response.body[1000]; // DATA NOT FOUND
                // console.log(response.body);

                expect(response.body.length).toBeGreaterThan(1);
                // TEST 1
                expect(firstResult).toHaveProperty("id", 1);
                expect(firstResult).toHaveProperty("title", "Masak");
                expect(firstResult).toHaveProperty("status", "active");
                // // TEST 2
                expect(secondResult).toBeUndefined();

                done();
            })
            .catch(done);
    });

    // GET To Do
    it("Detail Todo GET /todo/:id", (done) => {
        request(app)
            .get("/todo/3")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                const result = response.body;
                // console.log(result);
                expect(result).toHaveProperty("id", 3);
                expect(result).toHaveProperty("title", "Main Game");
                expect(result).toHaveProperty("status", "active");

                done();
            })
            .catch(done);
    });

    // POST To Do
    it("Create Todo POST /todo", (done) => {
        const createTodo = {
            title: "Berenang",
            status: "active",
        };

        request(app)
            .post("/todo")
            .send(createTodo)
            .expect("Content-Type", /json/)
            .set("Accept", "application/json")
            .expect(201)
            .then((response) => {
                const result = response.body;
                expect(result.message).toBe("Create To Do Successfully");
                expect(result.data).toHaveProperty("id", result.data.id);
                expect(result.data).toHaveProperty("title", createTodo.title);
                expect(result.data).toHaveProperty("status", createTodo.status);

                done();
            })
            .catch(done);
    });

    it("Soft Delete Todo DELETE /todo/:id", (done) => {
        request(app)
            .delete("/todo/4")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                const result = response.body;
                // console.log(result.message);
                expect(result.message).toBe("Deleted To Do Successfully");

                done();
            })
            .catch(done);
    });
});
