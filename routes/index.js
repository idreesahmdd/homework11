const express = require("express");
const router = express();
const todolistRouter = require("./todolist");

router.use(todolistRouter);

module.exports = router;
