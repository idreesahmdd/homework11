"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        const data = [
            {
                title: "Masak",
                status: "active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                title: "Mandi",
                status: "active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                title: "Main Game",
                status: "active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                title: "Nonton TV",
                status: "active",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ];
        await queryInterface.bulkInsert("Todos", data);

        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete("Todos", null, {});

        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    },
};
